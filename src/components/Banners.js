import React from 'react';
import './Banners.css';
import { Link } from 'react-router-dom';
import './Highlights.css';

const STYLES = ['btn--primary', 'btn--outline', 'btn--test'];

const SIZES = ['btn--medium', 'btn--large'];

export const Banners = ({
  children,
  type,
  onClick,
  buttonStyle,
  buttonSize
}) => {
  const checkButtonStyle = STYLES.includes(buttonStyle)
    ? buttonStyle
    : STYLES[0];

  const checkButtonSize = SIZES.includes(buttonSize) ? buttonSize : SIZES[0];

  return (
    <Link to={`/register`}  className='btn-mobile'>
       <button to={`/register`} class="custom-btn btn-12"><span>Register Now!</span><span>Interested?</span></button>
    </Link>
  );
};