import React from 'react';
import * as AiIcons from 'react-icons/ai';
import * as GiIcons from 'react-icons/gi';
import * as SiIcons from "react-icons/si";
import * as ImIcons from 'react-icons/im';


export const SidebarDataAdmin = [

{
    title: 'ARTech',
    path: '/',
    icon: <GiIcons.GiFireDash />,
    linkName: 'nav-text'
   
  },

  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiOutlineHome />,
    linkName: 'nav-text'
  },
  {
    title: 'Products',
    path: '/product',
    icon: <SiIcons.SiPicartoDotTv />,
    linkName: 'nav-text'
  },
  {
    title: 'Dashboard',
    path: '/dashboard',
    icon: <SiIcons.SiMicrosoftaccess />,
    linkName: 'nav-text'
  },
  {
    title: 'User Profile',
    path: '/profile',
    icon: <ImIcons.ImProfile />,
    linkName: 'nav-text'
  },
  {
    title: 'About Us',
    path: '/support',
    icon: <GiIcons.GiHelp />,
    linkName: 'nav-text',
  }

];