import React, { useState, useEffect } from 'react';
import { Modal, Form } from 'react-bootstrap';
import { useHistory} from 'react-router-dom';
import Swal from 'sweetalert2/src/sweetalert2.js'
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';




export default function AddCartButton({}){

		const history = useHistory();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	const [quantity, setQuantity] = useState('0');
	const [isActive, setIsActive] = useState(true);

  const [loginButton, setLoginButton] = useState(true);
  useEffect(() => {
			if(localStorage.accessToken == null) {
				setIsActive(false);
				setLoginButton(false);
			
			} else {
				setIsActive(true);
				setLoginButton(true);
				
			}	


	}, [quantity]);

	function addCart(e) {
		e.preventDefault();

		

		fetch(`http://localhost:4000/updateOrder/${localStorage.orderId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
			quantity: quantity,
			productId: productId
		})
	})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				if(data === true ) {

			Swal.fire({
			
			    icon: 'success',
			    iconHtml: '<i class="far fa-compass fa-spin"></i>',
			    text: "Success - Order Being Proccessed",

			    position: 'center',
			    showConfirmButton: false,
			    timer: 3000,
			    timerProgressBar: true,

			})
			.then(() => window.location.reload())
			
		} else {
			Swal.fire({
				
			    icon: 'error',
			    iconHtml: '<i class="fas fa-times fa-spin"></i>',
			    title: "Error When Placing Order",
			    text: "Network Error or the Product is not Available",

			    position: 'center',
			    showConfirmButton: false,
			    timer: 4000,
			    timerProgressBar: true,

			});
		}
		})

			setQuantity(0);




	}
const productId = localStorage.product;
	return(
<>

		 {
      loginButton ? 
      <Link style={{ textDecoration: 'none' }} variant="contained" color="secondary">
      <Button size="small" onClick={handleShow}  variant="outlined" color="secondary" align="center" >Add to Cart</Button></Link> 
      : 
      <Link style={{ textDecoration: 'none' }} variant="contained" color="secondary" >
      <Button size="small" onClick={handleShow}  variant="outlined" color="secondary" align="center" disabled >Login to Add-Cart</Button></Link>  
 }
	<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add To Cart</Modal.Title>
        </Modal.Header>

        <Modal.Body>
        	<form> 


        		<Form.Group>
        			<Typography variant="caption" color="inherit" component="p" align="justify">Product ID Code:</Typography>
        			<Form.Control
        				disabled type="text"
        				required
        				value={productId}

        				
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Typography variant="caption" color="inherit" component="p" align="justify">Quantity:</Typography>
        			<Form.Control
        				type="number"
        				placeholder="Enter Quantity"
        				min="0"
        				required
        				value={quantity}
        				onChange={e => setQuantity(e.target.value)} 
        			/>
        		</Form.Group>

        	</form>

        </Modal.Body>

        <Modal.Footer>
        	{
			isActive ? <Button color="primary" variant="contained" onClick={addCart} type="submit">Add</Button> 
			:<Button color="primary" variant="outlined" onClick={addCart} type="submit" disabled >Add</Button> 
	        }
          <Button color="secondary" variant="contained" align="left" onClick={handleClose}> Close</Button>
     
     	
        </Modal.Footer>
    </Modal>

</>

		)
}
