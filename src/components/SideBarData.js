import React from 'react';
import * as AiIcons from 'react-icons/ai';
import * as GiIcons from 'react-icons/gi';
import * as SiIcons from 'react-icons/si';
import * as RiIcons from 'react-icons/ri';



export const SidebarData = [

{
    title: 'ARTech',
    path: '/',
    icon: <GiIcons.GiFireDash />,
    linkName: 'nav-text'
  },

  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiOutlineHome />,
    linkName: 'nav-text'
  },
  {
    title: 'Products',
    path: '/product',
    icon: <SiIcons.SiPicartoDotTv />,
    linkName: 'nav-text'
  },
  {
    title: 'About Us',
    path: '/support',
    icon: <GiIcons.GiHelp />,
    linkName: 'nav-text'
  },
  {
    title: 'Register',
    path: '/register',
    icon: <AiIcons.AiOutlineForm />,
    linkName: 'nav-text'
  },
  {
    title: 'Login',
    path: '/login',
    icon: <RiIcons.RiUserAddLine />,
    linkName: 'nav-text'
  },

];