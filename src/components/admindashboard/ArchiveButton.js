import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';

export default function UpdateStatusButton({userId}){

	function switchAccessLevel(e) {
		e.preventDefault();

		fetch(`http://localhost:4000/SetUserAsAdmin/${userId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				if(data === true ) {

			Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Success",
    text: "User Access Level Updated",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
			.then(() => window.location.reload())
			
		} else {
			Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    title: "Error",
    text: "Something Went Wrong",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
  })
		}
		})

	}

	return(
	
 <Link onClick={switchAccessLevel} > 
         <Tooltip title="Set User To Admin">
          <SupervisorAccountIcon style={{ fontSize: 30 }} />
        </Tooltip>
        </Link>  
        
		)
}
