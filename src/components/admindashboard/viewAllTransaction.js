import React, { useContext, useEffect, useState } from "react";
import UserContext from '../../UserContext';
import { Table } from "react-bootstrap";
import Grid from "@material-ui/core/Grid";
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2/src/sweetalert2.js'
import '../../assets/css/OrderPage.css';
import ViewOrderButton from "../viewOrderId";
import {Accordion, Card,} from 'react-bootstrap';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import ArchiveOrderButton from './ArchiveOrder';
import { green } from '@material-ui/core/colors';
import ViewWeekIcon from '@material-ui/icons/ViewWeek';
import SwitchOrderButton from './SwitchOrderStatus';
import DelteOrderButton from './RemoveOrder';






export default function TransactionPage() {
  const history = useHistory();
  const [orderList, setOrderList] = useState([]);
  const [allCourses, setAllCourses] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch(`http://localhost:4000/getAllOrders`, {
      headers: {
          Authorization: `Bearer ${localStorage.accessToken}`
        }
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data === false) {

          Swal.fire({
/*    toast: true,*/
    icon: 'warning',
    iconHtml: '<i class="fas fa-sync-alt fa-spin"></i>',
    title: "Order History Empty",
  text: "Redirecting to the Product Page",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 6000,
    timerProgressBar: true,

  }).then(() => history.push('/products'))



        } else {


            setOrderList(
            data.map((userOrders) => {
                console.log(userOrders)
            return (
            
                  <tr align="center" key={userOrders._id}>
                <Typography variant="overline" color="primary" >  <td>{userOrders._id}</td></Typography>  
                  <Typography variant="overline" color="primary" >  <td>{userOrders.userId}</td></Typography>  
          
                  <td className={userOrders.isActive ? "text-success" : "text-warning" }>
                  {userOrders.isActive ? "Ongoing": "On hold"} </td>

{/*        
            <Link to={`/products/${userOrders._id}`} style={{ textDecoration: 'none' }} className={userOrders.orderStatus ? "text-success": "text-danger"}>             
<Tooltip title="Update Order">
 <IconButton color="primary"aria-label="share">
          <AddShoppingCartIcon style={{ color: green[500] }}/>
        </IconButton>
        </Tooltip>
              </Link>         
                    
              */}
              
    <Link to={`/transactions/${userOrders._id}`} style={{ textDecoration: 'none' }}>              
<Tooltip title="View Order Details">
 <IconButton color="primary"aria-label="share">
          <MenuOpenIcon />
        </IconButton>
        </Tooltip>
              </Link>
              
{<ArchiveOrderButton orderId={userOrders._id} />}
<SwitchOrderButton orderId={userOrders._id} />
<DelteOrderButton orderId={userOrders._id} />

                </tr>
              );
            })

          );


        }


  })



          
  }, []);

return (
      <><div className="Test">
        <h1 className="text-center mb-5">Transaction Records</h1>
        <Table className="TTable" striped bordered hover size="sm">
          <thead>
            <tr>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth><Typography variant="inherit" color="primary" >
 Order ID's </Typography> <ViewWeekIcon className="svg_icons" style={{ fontSize: 20 }} /> <Typography variant="inherit" color="secondary" >USER ID's </Typography> </Button>
              </th>

              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  Order Status </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth> More Options </Button>
              </th>
            </tr>
          </thead>
          <tbody>{orderList}</tbody>
        </Table>
        </div>
      </>
    );


} 


