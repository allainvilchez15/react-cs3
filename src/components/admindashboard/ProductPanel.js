import React, { useContext, useEffect, useState } from "react";
import UpdateStatusButton from './ArchiveProduct'
import SwitchStatusButton from './ReArchive'
import RemoveUserButton from './RemoveProduct'
import './UsersPanel.css'
import UserContext from "../../UserContext";
import { Link } from 'react-router-dom';
import 
{ 
  DataGrid,
  Button,
  Typography,
  Tooltip,
  IconButton, 
  Avatar,
  Box,
  Table
} 
from '@material-ui/core';
/*import { Table } from "react-bootstrap";*/
import { DeleteOutline} from "@material-ui/icons";
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import UpdateProduct from './UpdateProduct'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));






export default function ProductsPanel() {
const {user, setUser} = useContext(UserContext);  
const [allProducts, setAllProducts] = useState([]);
const classes = useStyles();
/*const [rows, setRows] = useState([]);
const [columns, setColumns] = useState([]);
*/


  useEffect(() => {
    fetchAllProducts();
    
     function fetchAllProducts(){
      fetch(`http://localhost:4000/getAllProduct`, {
      headers: {
          Authorization: `Bearer ${localStorage.accessToken}`
        }
    }).then((response) => response.json())
      .then((data) => {
        console.log(data);
        setAllProducts(
            data.map((products) => {
              return (
                <tr className="MytAble" variant="overline" key={products._id}>
 <Box fontStyle="italic" align="center" m={1}>

     
   

<Typography variant="overline">
                  <td >
                  <Avatar alt="Remy Sharp" src={products.productImage} />

                  {products._id} 
                  </td>

</Typography>
   </Box>

 <td align="center"><Typography color="default" variant="inherit">{products.name}</Typography></td>


                 
                 <td align="center"> <Typography color="default" variant="inherit">{products.category}</Typography></td>
                  <td align="center"><Typography color="error" variant="inherit"><span>&#8369;</span>{products.price}</Typography></td>
                  
                  <td align="center" className={products.isActive? "text-success": "text-danger"}>
                  {products.isActive ? "in-Stock" : "Out of Stock"}
                  </td>

                  <td>

                <Typography align="center">

             <UpdateStatusButton productId={products._id} />   
             <SwitchStatusButton productId={products._id} />
             <UpdateProduct productId={products._id} />
    
            


                       <RemoveUserButton productId={products._id} />
            </Typography>
                  </td>
                </tr>
              );
            })
          );





      })






    }
      
  }, [])

   




  return (
    <><div className="Test">
        <h1 className="text-center mb-5">ADMIN DASHBOARD</h1>
        <Table className="TTable" striped bordered hover size="sm">
          <thead>
            <tr>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth> PRODUCT LISTS </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  PRODUCT NAME </Button>
              </th>
               <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  CATEGORY </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  PRICE </Button>
              </th>
           
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  STATUS </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth> More Options </Button>
              </th>
            </tr>
          </thead>
          <tbody>{allProducts}</tbody>
        </Table>
        </div>
      </>
  );
}

