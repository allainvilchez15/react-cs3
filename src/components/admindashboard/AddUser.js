import React, { useContext, useEffect, useState } from "react";
import {Form} from 'react-bootstrap';
import UserContext from "../../UserContext";
import Swal from 'sweetalert2/src/sweetalert2'
import {useHistory} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import axios from 'axios';


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));


export default function AddProduct(){




const classes = useStyles();
const history = useHistory();
const [firstName, setfirstName] = useState('');
const [lastName, setlastName] = useState('');
const [email, setEmail] = useState('');
const [mobileNo, setMobileNo] = useState('');
const [password, setPassword] = useState('');
const [accesLevel, setAccesLevel] = useState('');
const { user } = useContext(UserContext);
const[addProductButton, setAddProductButton] = useState(false);
 const [productImage, setproductImage] = useState(null);

  const handleChange = (event) => {
    setAccesLevel(event.target.value);
  };
  const uploadImage =(event) =>{
  	setproductImage(event.target.files[0]);
  };

useEffect(() => {
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password !== '') {
				setAddProductButton(true);
			
			} else {
				setAddProductButton(false);
				
			}	


	}, [firstName, lastName, email, mobileNo, password]);


	function addUser(e){
		e.preventDefault(); 


		fetch('http://localhost:4000', {
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.accessToken}`
		},
		body: JSON.stringify({
     	 firstName: firstName,
      	 lastName: lastName,
      	 mobileNo: mobileNo,
      	 email: email,
      	 password: password,
      	 accesLevel: accesLevel
    })
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
				Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Success",
    text: "User Created",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
		});

			setfirstName('');
			setlastName('');
			setEmail('');
			setMobileNo('');
			setPassword('');

			
		
	}
	if(user.isAdmin !== true) {
		history.push('/')
	}

	return(

	<form onSubmit={(e) => addUser(e)}>
	     <div className={classes.root}>
	<FormControl align="justify" fullWidth variant="outline" component="fieldset">
	<h1 className="text-center mb-5">Create User</h1>
		<Form.Group controlId="userFName">
			<Form.Label> <Typography variant="borderline" color="secondary" align="left">
			Enter First Name:
			</Typography></Form.Label>
			<Form.Control 
			size="sm"
			type="firstName" 
			placeholder="Enter First Name" 
			value={firstName} 
			onChange={e => setfirstName(e.target.value)} 
/*			required */
			/>
		</Form.Group>

		<Form.Group controlId="userLName">
			<Form.Label> <Typography variant="borderline" color="secondary" align="left">
			Enter Last Name
			</Typography></Form.Label>
			<Form.Control 
			size="sm"
			type="lastName" 
			placeholder="Enter Last Name" 
			value={lastName} 
			onChange={e => setlastName(e.target.value)} 
	/*		required */
			/>
		</Form.Group>

		<Form.Group controlId="userEmail">
			<Form.Label><Typography variant="borderline" color="secondary" align="left">Enter email:</Typography></Form.Label>
			<Typography variant="borderline" color="secondary" align="left"><Form.Control 
			type="email" 
			placeholder="Enter Product Long Description" 
			value={email} 
			onChange={e => setEmail(e.target.value)} 
			/*required */
			/></Typography>
		
		</Form.Group>

		<Form.Group controlId="userMobileNo">
			<Form.Label><Typography variant="borderline" color="secondary" align="left">Mobile No:</Typography></Form.Label>
			<Form.Control 
			type="mobileNo" 
			placeholder="Enter Mobile Number" 
			value={mobileNo} 
			onChange={e => setMobileNo(e.target.value)} 
		/*	required */
			/>
		
		</Form.Group>
		<Form.Group controlId="userPassword">
			<Form.Label><Typography variant="borderline" color="secondary" align="left">Password</Typography></Form.Label>
			<Form.Control 
			type="password" 
			placeholder="Enter Password" 
			value={password} 
			onChange={e => setPassword(e.target.value)} 
		/*	required */
			/>
		
		</Form.Group>

		    
     
      <RadioGroup value={accesLevel} onChange={handleChange}>
        <FormControlLabel value=" basic" control={<Radio />} label="Basic" />
         <FormControlLabel value="employee" disabled control={<Radio />} label="Employee" />
        <FormControlLabel value="admin" control={<Radio />} label="Admin" />
        <FormControlLabel value="admin" disabled control={<Radio />} label="Super Admin" />


      </RadioGroup>


    
      <input
        accept="image/*"
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
/*        onChange={uploadImage}*/
   
      />
      <label htmlFor="contained-button-file">
        <Button variant="outlined" color="primary" component="span">
         Avatar Photo
        </Button>
      </label>


		{


    
			{/*addProductButton */}? <Button align="right" variant="outlined" color="secondary" type="submit">Create User</Button> 
			: <Button variant="primary" type="submit" disabled >Create User</Button>
		
		}
    </FormControl>
     </div>
	</form>

		)

}

