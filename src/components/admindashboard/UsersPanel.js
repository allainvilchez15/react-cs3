import React, { useContext, useEffect, useState } from "react";
import UpdateStatusButton from './ArchiveButton'
import SwitchStatusButton from './NOTArchiveButton'
import RemoveUserButton from './RemoveUser'
import './UsersPanel.css'
import UserContext from "../../UserContext";
import { Link } from 'react-router-dom';
import 
{ 
  DataGrid,
  Button,
  Typography,
  Tooltip,
  IconButton, 
  Avatar,
  Box,
  Table
} 
from '@material-ui/core';
/*import { Table } from "react-bootstrap";*/
import { DeleteOutline} from "@material-ui/icons";
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { green } from '@material-ui/core/colors';
import UpdateProfile from './UpdateProfile'







export default function UsersPanel() {
const {user, setUser} = useContext(UserContext);  
const [allUsers, setAllUsers] = useState([]);
/*const [rows, setRows] = useState([]);
const [columns, setColumns] = useState([]);
*/


  useEffect(() => {
    fetchAllUsers();
    
     function fetchAllUsers(){
      fetch(`http://localhost:4000/getAllUsers`, {
      headers: {
          Authorization: `Bearer ${localStorage.accessToken}`
        }
    }).then((response) => response.json())
      .then((data) => {
        console.log(data);
        setAllUsers(
            data.map((users) => {
              return (
                <tr className="MytAble" variant="overline" key={users._id}>
 <Box fontStyle="italic" align="center" m={1}>

     
   

<Typography variant="overline">
                  <td >
                  <Tooltip title="User Avatar">
                  <PersonOutlineIcon color="inherit" fontSize="small" /></Tooltip>
                  {users._id} 
                  </td>
</Typography>
   </Box>




                  <td align="center">{users.firstName}</td>
                  <td align="center">{users.lastName}</td>
                  <td align="center">{users.firstName} {users.lastName}</td>
                  <td align="center">{users.email}</td>
                  <td align="center" className={users.isAdmin? "text-success": "text-danger"}>
                  {users.isAdmin ? "Admin" : "Basic"}
                  </td>
                  <td>

                <Typography align="center">

             <UpdateStatusButton userId={users._id} />   
             <SwitchStatusButton userId={users._id} />
             <UpdateProfile userId={users._id} />

                       <RemoveUserButton userId={users._id} />
            </Typography>
                  </td>
                </tr>
              );
            })
          );





      })






    }
      
  }, [])

   




  return (
    <><div className="Test">
        <h1 className="text-center mb-5">ADMIN DASHBOARD</h1>
        <Table className="TTable" striped bordered hover size="sm">
          <thead>
            <tr>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth> USER ID's </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  FIRSTNAME </Button>
              </th>
               <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  LASTNAME </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  FULLNAME </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  EMAIL </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>  ACCESS LEVEL </Button>
              </th>
              <th>
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth> More Options </Button>
              </th>
            </tr>
          </thead>
          <tbody>{allUsers}</tbody>
        </Table>
        </div>
      </>
  );
}