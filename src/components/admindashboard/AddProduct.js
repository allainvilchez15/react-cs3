import React, { useContext, useEffect, useState } from "react";
import {Form} from 'react-bootstrap';
import UserContext from "../../UserContext";
import Swal from 'sweetalert2/src/sweetalert2'
import {useHistory} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import axios from 'axios';


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));


export default function AddProduct(){




const classes = useStyles();
const history = useHistory();
const [name, setName] = useState('');
const [category, setCategory] = useState('');
const [description, setDescription] = useState('');
const [highlightText, setHighlightText] = useState('');
const [price, setPrice] = useState(0);
const { user } = useContext(UserContext);
const[addProductButton, setAddProductButton] = useState(false);
 const [productImage, setproductImage] = useState(null);

  const handleChange = (event) => {
    setCategory(event.target.value);
  };
  const uploadImage =(event) =>{
  	setproductImage(event.target.files[0]);
  };

useEffect(() => {
		if(name !== '' && description !== '' && price !== '') {
				setAddProductButton(true);
			
			} else {
				setAddProductButton(false);
				
			}	


	}, [name, description, price]);


	function addProduct(e){
		e.preventDefault(); 


		fetch('http://localhost:4000/createProductCPU', {
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.accessToken}`
		},
		body: JSON.stringify({
			name: name,
			highlightText: highlightText,
			description: description,
			price: price,
			category: category,
		/*	productImage: productImage,*/
		})
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
				Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Success",
    text: "Product Added",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
		});

			setName('');
			setDescription('');
			setPrice(0);
		
	}
	if(user.isAdmin !== true) {
		history.push('/')
	}

	return(

	<form onSubmit={(e) => addProduct(e)}>
	     <div className={classes.root}>
	<FormControl align="justify" fullWidth variant="outline" component="fieldset">
	<h1 className="text-center mb-5">Add Product</h1>
		<Form.Group controlId="courseName">
			<Form.Label> <Typography variant="borderline" color="secondary" align="left">
			Product Name:
			</Typography></Form.Label>
			<Form.Control 
			size="sm"
			type="name" 
			placeholder="Enter Product Name" 
			value={name} 
			onChange={e => setName(e.target.value)} 
			required 
			/>
		</Form.Group>

		<Form.Group controlId="courseShortText">
			<Form.Label> <Typography variant="borderline" color="secondary" align="left">
			Product Short Description:
			</Typography></Form.Label>
			<Form.Control 
			size="sm"
			type="highlightText" 
			placeholder="Enter Short Description" 
			value={highlightText} 
			onChange={e => setHighlightText(e.target.value)} 
			required 
			/>
		</Form.Group>

		<Form.Group controlId="courseDescription">
			<Form.Label><Typography variant="borderline" color="secondary" align="left">Product Long Description:</Typography></Form.Label>
			<Typography variant="borderline" color="secondary" align="left"><Form.Control 
			as="textarea" 
			type="description" 
			placeholder="Enter Product Long Description" 
			rows={3} value={description} 
			onChange={e => setDescription(e.target.value)} 
			required 
			/></Typography>
		
		</Form.Group>

		<Form.Group controlId="coursePrice">
			<Form.Label><Typography variant="borderline" color="secondary" align="left">Product Price:</Typography></Form.Label>
			<Form.Control 
			type="number" 
			placeholder="Enter Product Price" 
			value={price} 
			onChange={e => setPrice(e.target.value)} 
			required 
			min="0"
			/>
		
		</Form.Group>

		    
     
      <RadioGroup value={category} onChange={handleChange}>
        <FormControlLabel value="GPU" control={<Radio />} label="Video Card" />
        <FormControlLabel value="MOBO" control={<Radio />} label="Motherboard" />
        <FormControlLabel value="CPU" control={<Radio />} label="CPU" />
        <FormControlLabel value="disabled" disabled control={<Radio />} label="RAM" />

      </RadioGroup>


    
      <input
        accept="image/*"
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
   
        onChange={e => setproductImage(e.target.files[0])} 
   
      />
      <label htmlFor="contained-button-file">
        <Button variant="outlined" color="primary" component="span">
          ProductImage
        </Button>
      </label>


		{


    
			{/*addProductButton */}? <Button align="right" variant="outlined" color="secondary" type="submit">Add Product</Button> 
			: <Button variant="primary" type="submit" disabled >Add Product </Button>
		
		}
    </FormControl>
     </div>
	</form>

		)

}

