import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import { DeleteOutline } from "@material-ui/icons";
import SwitchCameraIcon from '@material-ui/icons/SwitchCamera';

export default function UpdateStatusButton({orderId}){

	function archiveOrder(e) {
		e.preventDefault();

		fetch(`http://localhost:4000/ReArchiveOrder/${orderId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				if(data === true ) {

			Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Success",
    text: "Order Cancelled",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
			.then(() => window.location.reload())
			
		} else {
			Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    title: "Error - Order is being Process already",
    text: "Only Store Admin can Cancel your Orders",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
  })
		}
		})

	}

	return(
	
 <Link onClick={archiveOrder} > 
         <Tooltip title="Set Order to On hold">
          <SwitchCameraIcon color="error" />
        </Tooltip>
        </Link>  
        
		)
}
