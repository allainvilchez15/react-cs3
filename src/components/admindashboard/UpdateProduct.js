import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { Link, useHistory} from 'react-router-dom';
import Swal from 'sweetalert2';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { green } from '@material-ui/core/colors';
import Tooltip from '@material-ui/core/Tooltip';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';


export default function UpdateButton({productId}) {

	const history = useHistory();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	//forms
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [highlightText, sethighlightText] = useState('');
	const [category, setCategory] = useState('');
	const [price, setPrice] = useState('0');
	const [isActive, setIsActive] = useState(true);

	const handleChange = (event) => {
    setCategory(event.target.value);
  };


	useEffect(() => {
			if(name !== '' && description !== '' && price !== '') {
				setIsActive(true);
			
			} else {
				setIsActive(false);
				
			}	


	}, [name, description, price]);

	function updateProduct(e) {

		e.preventDefault(); 


		fetch(`http://localhost:4000/updateProduct/${productId}`, {
		method: 'PUT',
		headers: { 
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.accessToken}`
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price,
			category: category,
			highlightText: highlightText
		})
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
			if(data === true ) {

			Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Success",
    text: "Product Updated",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
			.then(() => window.location.reload())
			
		} else {
			Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    title: "Error",
    text: "Something Went Wrong",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
  })
			.then(() => window.location.reload())
		}


		})


	}

	

	return(
<>

 <Link onClick={handleShow} > 
         <Tooltip title="Update Product" >
          <ListAltIcon style={{ fontSize: 30, color: green[500] }}/>
        </Tooltip>
        </Link>  

		
	<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title><Typography variant="borderline" color="secondary" align="left">
Update Product Form</Typography></Modal.Title>
        </Modal.Header>

        <Modal.Body>
        	<form> 
        		<Form.Group>
        			<Form.Label><Typography variant="borderline" color="secondary" align="left">Name:</Typography></Form.Label>
        			<Form.Control
        				type="text"
        				placeholder="Enter Updated Product Name"
        				required 
        				value={name}
        				onChange={e => setName(e.target.value)} 
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label><Typography variant="borderline" color="secondary" align="left">Short Description:</Typography></Form.Label>
        			<Form.Control
        				type="text"
        				placeholder="Enter Updated Short Description"
        				required 
        				value={highlightText}
        				onChange={e => sethighlightText(e.target.value)} 
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label><Typography variant="borderline" color="secondary" align="left">Description:</Typography></Form.Label>
        			<Form.Control
        				as="textarea"
        				rows={3}
        				type="text"
        				placeholder="Enter Updated Description"
        				required
        				value={description}
        				onChange={e => setDescription(e.target.value)} 
        			/>
        		</Form.Group>


        		<Form.Group>
        			<Form.Label><Typography variant="borderline" color="secondary" align="left">Price:</Typography></Form.Label>
        			<Form.Control
        				type="number"
        				placeholder="Enter Updated Price"
        				required
        				value={price}
        				onChange={e => setPrice(e.target.value)} 
        			/>
        		</Form.Group>

                <RadioGroup value={category} onChange={handleChange}>
        <FormControlLabel value="GPU" control={<Radio />} label="Video Card" />
        <FormControlLabel value="MOBO" control={<Radio />} label="Motherboard" />
        <FormControlLabel value="CPU" control={<Radio />} label="CPU" />
        <FormControlLabel value="disabled" disabled control={<Radio />} label="RAM" />

      </RadioGroup>

        	</form>

        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
          {
			isActive ? <Button variant="primary" onClick={updateProduct} type="submit">Save Changes</Button> 
			: <Button variant="primary" type="submit" disabled >Save Changes</Button> 
	      }

        </Modal.Footer>
    </Modal>

</>

		)

}