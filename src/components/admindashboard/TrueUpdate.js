import React, {useState} from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import { DeleteOutline } from "@material-ui/icons";
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import {useHistory} from 'react-router-dom';


export default function UpdateStatusButton({orderId}){
		const history = useHistory();

	function updateOrder(e) {
		e.preventDefault();

		fetch(`http://localhost:4000/getSpecificOrder/${orderId}`, {
				headers: {
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			}

	})
		.then(res => res.json())
		.then(data => {
			console.log(data)
	if (data.isActive === false) {
	
		localStorage.removeItem('orderId', data._id);
		localStorage.setItem('orderId', orderId);

		history.push('/products')
	


		} else {


		Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    title: "Error - Order is being Process already",
    text: "Only Store Admin can Cancel your Orders",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
  })

		.then(() => window.location.reload())



		}

		})
	



	}

	return(
	
 <Link onClick={updateOrder} > 
         <Tooltip title="Update Order">
          <AddShoppingCartIcon color="default" style={{ fontSize: 30 }} />
        </Tooltip>
        </Link>  
        
		)
}
