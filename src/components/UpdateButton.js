import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useHistory} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function UpdteButton({productId}) {

	const history = useHistory();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	//forms
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('0');
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
			if(name !== '' && description !== '' && price !== '') {
				setIsActive(true);
			
			} else {
				setIsActive(false);
				
			}	


	}, [name, description, price]);

	function updateProduct(e) {

		e.preventDefault(); 


		fetch(`http://localhost:4000/updateProduct/${productId}`, {
		method: 'PUT',
		headers: { 
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.accessToken}`
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
			if(data === true ) {

			Swal.fire({
				title: "Success",
				icon: "success",
				text: "The course is Updated"

			})
			.then(() => history.push(0))
			
		} else {
			Swal.fire({
				title: "Error",
				icon: "error",
				text: "Failed to Update the course"

			});
		}


		})


	}

	

	return(
<>
<Button variant="primary" onClick={handleShow}>Update</Button>
		
	<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update a Course</Modal.Title>
        </Modal.Header>

        <Modal.Body>
        	<form> 
        		<Form.Group>
        			<Form.Label>Name:</Form.Label>
        			<Form.Control
        				type="text"
        				placeholder="Enter Updated Course Name"
        				required 
        				value={name}
        				onChange={e => setName(e.target.value)} 
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label>Description:</Form.Label>
        			<Form.Control
        				as="textarea"
        				rows={3}
        				type="text"
        				placeholder="Enter Updated Description"
        				required
        				value={description}
        				onChange={e => setDescription(e.target.value)} 
        			/>
        		</Form.Group>

        		<Form.Group>
        			<Form.Label>Price:</Form.Label>
        			<Form.Control
        				type="number"
        				placeholder="Enter Updated Price"
        				required
        				value={price}
        				onChange={e => setPrice(e.target.value)} 
        			/>
        		</Form.Group>

        	</form>

        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
          {
			isActive ? <Button variant="primary" onClick={updateProduct} type="submit">Save Changes</Button> 
			: <Button variant="primary" type="submit" disabled >Save Changes</Button> 
	      }

        </Modal.Footer>
    </Modal>

</>

		)

}