import React, { useState, useEffect } from 'react';
import { Modal, Form } from 'react-bootstrap';
import { useHistory} from 'react-router-dom';
import Swal from 'sweetalert2/src/sweetalert2.js'
import Button from '@material-ui/core/Button';
import JSONPretty from 'react-json-prettify';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import CardHeader from '@material-ui/core/CardHeader';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import InfoIcon from '@material-ui/icons/Info';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Link } from 'react-router-dom';
import {atomOneLight} from 'react-json-prettify/dist/themes';
import PrintIcon from '@material-ui/icons/Print';
import '../assets/css/OrderPage.css';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Tooltip from '@material-ui/core/Tooltip';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: '50%',
     flexGrow: 1,
     marginBottom: 100,
  },
     Card: {
    width: 300,
    margin: 'auto'
  },
  Media: {
    height: 550,
    width: '100%'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function ViewTransaction({ match }) {

  const history = useHistory();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
 const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [transaction, setTransaction] = useState({ 
    productImage: {}
  });

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    fetchTransaction();
    
    
      
  }, [])




  function fetchTransaction(){


    fetch(`http://localhost:4000/getUsersSpecificOrder/${match.params.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.accessToken}`
        }
      })
    .then(response => response.json())
    .then(data => {
      setTransaction(data);
      console.log(data)
localStorage.removeItem('orderId', data._id);
localStorage.setItem('orderId', data._id);

    }) 
  /*  return (

     
      )*/

  }
/*  const image = product.productImage;*/


  return(
<>

<div className="TRecord">
 <Grid container spacing={3}>
         <Grid item xs={12} sm={12} align="center">
 <Card className={classes.root}>
    


      <CardActions disableSpacing>

   
        <IconButton aria-label="info">

        <Link to={`/products/`} style={{ textDecoration: 'none' }}> 
         <Tooltip title="Update Order">
          <AddShoppingCartIcon style={{ color: green[500] }}/>
        </Tooltip>
        </Link>  

          <Tooltip title="Print Document">
             <PrintIcon color="secondary"/>
               </Tooltip>
                <Tooltip title="More Info">
          <InfoIcon color="primary"/>
           </Tooltip>
        </IconButton>
  

          <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled fullWidth>   <Typography variant="borderline" color="primary" align="center">
             Order With ID : {transaction._id}
              </Typography> </Button>
       
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
  

    
          <Typography variant="borderline" color="secondary" align="left">
          <JSONPretty json={transaction} theme={atomOneLight} padding={4} />

          </Typography>
   
        </CardContent>
      </Collapse>
{/*        <td className={product.isActive ? "text-success" : "text-danger"}>
    {product.isActive ? "Available": "Out of Stock"} </td>

  <AddCartButton courseId={product._id} />

*/}
    </Card>
</Grid>
  </Grid>


</div>
</>

    )

}