import React, { useState, useEffect } from 'react';
import { Modal, Form } from 'react-bootstrap';
import { useHistory} from 'react-router-dom';
import Swal from 'sweetalert2/src/sweetalert2.js'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import CardHeader from '@material-ui/core/CardHeader';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Link } from 'react-router-dom';
import AddCartButton from "./AddCart1";
import '../assets/css/product.css';


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: '50%',
     flexGrow: 1,
     marginBottom: 100,
  },
     Card: {
    width: 300,
    margin: 'auto'
  },
  Media: {
    height: 550,
    width: '100%'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function ViewProduct({ match }) {

	const history = useHistory();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
 const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [product, setProduct] = useState({ 
    productImage: {}
  });

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    fetchProduct();
    
    
      
  }, [])




	function fetchProduct(){


		fetch(`http://localhost:4000/getSpecificProduct/${match.params.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.accessToken}`
        }
      })
		.then(response => response.json())
		.then(data => {
			setProduct(data);
			console.log(data)
localStorage.removeItem('product', data._id);
localStorage.setItem('product', data._id);


		}) 
	/*	return (

     
			)*/

	}
/*	const image = product.productImage;*/


	return(
<>

<div className="FooterFIx">
 <Grid container spacing={3}>
         <Grid item xs={12} sm={12} align="center">
 <Card className={classes.root}>
      <CardHeader align="left"

     
        title={product.name}
        subheader={product.highlightText}

      />

             <Typography variant="body1" color="error" component="p" align="left"> 
            <span>&#8369;</span> {product.price} 
          </Typography>
       <img className="PHOT1" src={product.productImage} />
      <CardContent>
      
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
  

    
          <Typography variant="body2" color="textSecondary" component="p" align="left">
           {product.description}
          </Typography>
   
        </CardContent>
      </Collapse>
      	<td className={product.isActive ? "text-success" : "text-danger"}>
		{product.isActive ? "Available": "Out of Stock"} </td>

  <AddCartButton courseId={product._id} />


    </Card>
</Grid>
  </Grid>

</div>

</>

		)

}