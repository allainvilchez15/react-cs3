import React from 'react';
import '../App.css';
import { Banners } from './Banners';


function Highlights() {
  return (
    <div className='hero-container'>
      <video src='/videos/video.mp4' autoPlay loop muted />


      <h1>____|START SHOPPING|____</h1>
      <p>Come Spend Your Money on Us</p>
      <div className='hero-btns'>
        <Banners
          className='btns'
          buttonStyle='btn--outline'
          buttonSize='btn--large'
        >
        </Banners>

      </div>

    </div>
  );
}

export default Highlights;