import React, { useState, useEffect, useContext } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { Link, useHistory} from 'react-router-dom';
import Swal from 'sweetalert2';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { green } from '@material-ui/core/colors';
import Tooltip from '@material-ui/core/Tooltip';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import UserContext from '../../UserContext';


export default function UpdateButton({userId}) {

	const history = useHistory();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const {user, setUser, unsetUser} = useContext(UserContext);


	//forms
	const [firstName, setfirstName] = useState('');
	const [lastName, setlastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [isActive, setIsActive] = useState(true);
/*
	const handleChange = (event) => {
    setCategory(event.target.value);
  };
*/

	useEffect(() => {
	/*		if(firstName !== '' && lastName !== '' && email !== '' && password !== '' && mobileNo !== '') {
				setIsActive(true);
			
			} else {
				setIsActive(false);
				
			}	*/


	}, [firstName, lastName, email, password, mobileNo]);

	function updateProfile(e) {

		e.preventDefault(); 


		fetch(`http://localhost:4000/updateProfile/${localStorage.id}`, {
		method: 'PUT',
		headers: { 
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.accessToken}`
		},
		body: JSON.stringify({
	

            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            mobileNo: mobileNo

	})
        })
	.then(response => response.json())
	.then(data => {
		console.log(data)
			if(data === true ) {

			Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Success",
    text: "User Profile Updated",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
			.then(() => {

    unsetUser();
    setUser({accessToken: null})
    window.localStorage.clear()

			}).then(() => history.push('/login'))
			
		} else {
			Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    title: "Error",
    text: "Something Went Wrong",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
  })
			.then(() => window.location.reload())
		}


		})


	}



	return(
<div className="UpdateProf">
<form  onSubmit={(e) => updateProfile(e)} > 
        		<Form.Group>
        			<Form.Label><Typography fullWidth variant="capton" color="secondary" align="left">First Name:</Typography></Form.Label>
        			<Form.Control
        				type="text"
        				/*placeholder="Enter Updated First Name"*/
        				
        				value={firstName}
        				onChange={e => setfirstName(e.target.value)} 
        			/>
        		</Form.Group>

                <Form.Group>
                    <Form.Label><Typography variant="borderline" color="secondary" align="left">Last Name:</Typography></Form.Label>
                    <Form.Control
                        type="text"
                      /*  placeholder="Enter Updated Last Name"*/
                        
                        value={lastName}
                        onChange={e => setlastName(e.target.value)} 
                    />
                </Form.Group>


        		<Form.Group>
        			<Form.Label><Typography variant="borderline" color="secondary" align="left">Email:</Typography></Form.Label>
        			<Form.Control
        				type="email"
        				/*placeholder="Enter new Email"*/
        				
        				value={email}
        				onChange={e => setEmail(e.target.value)} 
        			/>
        		</Form.Group>

                <Form.Group>
                    <Form.Label><Typography variant="borderline" color="secondary" align="left">Mobile Number:</Typography></Form.Label>
                    <Form.Control
                        type="mobileNo"
                      /*  placeholder="Enter Updated Mobile Number"*/
                        
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)} 
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label><Typography variant="borderline" color="secondary" align="left">Password:</Typography></Form.Label>
                    <Form.Control
                        type="password"
              /*          placeholder="Enter New Password"*/
                        
                        value={password}
                        onChange={e => setPassword(e.target.value)} 
                    />
                </Form.Group>


        

		{


    
			{setIsActive }? <Button align="right" variant="outlined" color="secondary" type="submit">Update Profile</Button> 
			: <Button variant="primary" type="submit" disabled >Update Profile </Button>
		
		}
        

        	</form>

	</div>

		)

}

