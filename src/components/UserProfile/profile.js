import React, {useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Profile from '../../assets/images/BH.jpg'
import './profile.css';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Update from './Update';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';


function TabPanel(props) {
  const { children, value, index, ...other } = props;


  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    marginLeft: '200',
    align: 'left',
  },
    tabBar: {
      top: '80px'
    },

}));

export default function ImgMediaCard() {
 const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const [userProfile, setuserProfile] = useState([]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };



  useEffect(() => {
    fetchUser();
    
    
      
  }, [])




  function fetchUser(){


    fetch(`http://localhost:4000/detailsUser/${localStorage.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.accessToken}`
        }
      })
    .then(response => response.json())
    .then(data => {
      setuserProfile(data);
      console.log(userProfile)


    }) 

  }

  return (
    <>
    <div className="PP">
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
      
          component="img"
          alt="Contemplative Reptile"
          height="240"
          image={Profile}
          title="Contemplative Reptile"
        />

           
      </CardActionArea>
    </Card>


            <CardContent>

          <Typography gutterBottom variant="contained" component="h2">
          <AccountCircleIcon className="Avatar"/>
          </Typography>
                   
              <Button variant="contained" color="primary" component="p" align="left" value="blocked" align="justify" disabled >   <Typography variant="borderline" color="secondary" align="center">
           Hi! {localStorage.firstName} {localStorage.lastName}
              </Typography> </Button>
        </CardContent>
         <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
        align="left"
      >
        <Tab color="primary" label="Profile" {...a11yProps(0)} />
        <Tab label="Favorite" {...a11yProps(1)} />
        <Tab label="Profile Settings" {...a11yProps(2)} />

      </Tabs>
      <TabPanel value={value} index={0}>
     <Typography variant="subtitle1" color="secondary" align="left">     Account Information</Typography>
   
            <Typography variant="overline" color="inherit" align="center">{userProfile.firstName} {userProfile.lastName}</Typography>
            <Typography variant="subtitle1" color="secondary" align="left">   Mobile Number</Typography>
                <Typography variant="overline" color="inherit" align="left">{userProfile.mobileNo}</Typography>
                <Typography variant="subtitle1" color="secondary" align="left">     Email</Typography>
                <Typography variant="overline" color="inherit" align="left">{userProfile.email}</Typography>
                <Typography variant="subtitle1" color="secondary" align="left">     Account Type</Typography>
                <Typography variant="overline" color="inherit" align="left">{userProfile.accessLevel}</Typography>
                <Typography variant="subtitle1" color="secondary" align="left">     Customer Store ID</Typography>
                <Typography variant="overline" color="inherit" align="left">{userProfile._id}</Typography>

      </TabPanel>
      <TabPanel value={value} index={1}>
             <CardActionArea>

          <Typography variant="body2" color="textSecondary" align="right">
          Lotione rerum a suscipit alias?
          </Typography>


           
      </CardActionArea>

      </TabPanel>
      <TabPanel value={value} index={2}>
             <Update /*userId={users._id}*/ />
      </TabPanel>
   
    </div>


         
              </div>



            

<div className="Profile-Bar">

    </div>




</>

  );
}
