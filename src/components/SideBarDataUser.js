import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as GiIcons from 'react-icons/gi'
import * as SiIcons from "react-icons/si";
import * as GrIcons from "react-icons/gr";
import * as RiIcons from 'react-icons/ri';
import * as ImIcons from 'react-icons/im';
import * as TiIcons from 'react-icons/ti';

export const SidebarDataUser = [

{
    title: 'ARTech',
    path: '/',
    icon: <GiIcons.GiFireDash />,
    linkName: 'nav-text'
  },

  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiOutlineHome />,
    linkName: 'nav-text'
  },
  {
    title: 'Products',
    path: '/product',
    icon: <SiIcons.SiPicartoDotTv />,
    linkName: 'nav-text'
  },
  {
    title: 'User Profile',
    path: '/profile',
    icon: <ImIcons.ImProfile />,
    linkName: 'nav-text'
  },
  {
    title: 'Transactions',
    path: '/transactions',
    icon: <TiIcons.TiShoppingCart />,
    linkName: 'nav-text'
  },
  {
    title: 'About Us',
    path: '/support',
    icon: <GiIcons.GiHelp />,
    linkName: 'nav-text'
  },


];