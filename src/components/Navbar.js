import React, { useState, useContext } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import UserContext from '../UserContext';
import { Link, useHistory } from 'react-router-dom';
import { SidebarData } from './SideBarData';
import { SidebarDataAdmin } from './SideBarDataAdmin';
import { SidebarDataUser } from './SideBarDataUser';
import './Navbar.css';
import { IconContext } from 'react-icons';
import * as GiIcons from 'react-icons/gi'
import* as CgIcons from "react-icons/cg";
import Button from '@material-ui/core/Button';
import Swal from 'sweetalert2/src/sweetalert2.js'

function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const history = useHistory();
  const showSidebar = () => setSidebar(!sidebar);

const {user, setUser, unsetUser} = useContext(UserContext);


  const logout = () => {
    unsetUser();
    setUser({accessToken: null})
    window.localStorage.clear()
     Swal.fire({
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    text: "User Logout - Redirecting",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,

  }).then(() => history.push('/login'))
  }

/*ADMINUSER*/
if (user.isAdmin === true) {
return (
    
      <IconContext.Provider value={{ color: '#fff' }}>
        <div className='navbar'>
          <Link to='#' className='menu-bars'>
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>
         <Link onClick={() => history.goBack()} className='navbar-logo' >
            ARTech
            <GiIcons.GiFireDash />
          </Link>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars'>
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
             <div className='navbar-user' >
            <i className="far fa-user"></i>
            {localStorage.firstName}
            </div>
            {SidebarDataAdmin.map((item, index) => {
              return (
                <li key={index} className={item.linkName}>
                  <Link to={item.path}>
                    {item.icon}
                    <span>{item.title}</span>
                  </Link>
                </li>
              );
            })}
            <Link to='' onClick={logout} className='navbar-logout' >
            <CgIcons.CgLogOut />
            Logout
            </Link>
          </ul>
        </nav>
      </IconContext.Provider>

  );

  /*REGULARUSER*/
} if ((user.accessToken !== null) && (user.isAdmin === false)) {

return (
    
      <IconContext.Provider value={{ color: '#fff' }}>
        <div className='navbar'>
          <Link to='#' className='menu-bars'>
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>
          <Link onClick={() => history.goBack()} className='navbar-logo' >
            ARTech
            <GiIcons.GiFireDash />
          </Link>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars'>
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
             <div className='navbar-user' >
            <i className="far fa-user"></i>
            {localStorage.firstName}
            </div>
            {SidebarDataUser.map((item, index) => {
              return (
                <li key={index} className={item.linkName}>
                  <Link to={item.path}>
                    {item.icon}
                    <span>{item.title}</span>
                  </Link>
                </li>
              );
            })}
            <Link to='' onClick={logout} className='navbar-logout' >
            <CgIcons.CgLogOut />
            Logout
            </Link>
          </ul>
        </nav>
      </IconContext.Provider>

  );

} else {
return (
    
      <IconContext.Provider value={{ color: '#fff' }}>
        <div className='navbar'>
          <Link to='#' className='menu-bars'>
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>
          <Link onClick={() => history.goBack()} className='navbar-logo' >
            ARTech
            <GiIcons.GiFireDash />
          </Link>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars'>
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {SidebarData.map((item, index) => {
              return (
                <li key={index} className={item.linkName}>
                  <Link to={item.path}>
                    {item.icon}
                    <span>{item.title}</span>
                  </Link>
                </li>
              );
            })}
          </ul>
        </nav>
      </IconContext.Provider>

  );


}

}

export default Navbar;