
import React, {useState, useEffect} from 'react';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import '../components/product.css';
import P1 from '../assets/images/Products/G1.jpg';
import V1 from '../assets/images/video/axial-fans.mp4'
import ViewButton from "../components/ViewProduct";
/*import Paper from '@material-ui/core/Paper';*/


const useStyles = makeStyles((theme) => ({
  root: {
/*    flexGrow: 1,*/
     margin: theme.spacing(1),
  },
   container: {
    display: "flex"
  },
   media: {
    height: 75,
    width: 85,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
      paper: {
    height: 200,
    flex: 1,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    elevation: 8
  }
  },

}));

export default function GpuProducts({gpuProduct, gpuProductId}) {
   const classes = useStyles();
  const[isOpen, setIsOpen] = useState(true);

    function getAllMobo(gpuProductId){


    fetch(`http://localhost:4000/getAllGPU`, {
      headers: {
        Authorization: `Bearer $(user.accessToken)`,
      },
    })
    .then(response => response.json())
    .then(data => {
      console.log(data)
    

    }) 

  }





  return (

<>


      <Grid container spacing={3}>
         <Grid item xs={12} sm={12} align="center">

          <Card className={classes.root}>
      <CardActionArea >
     
  <img className="PHOT" src={gpuProduct.productImage} />
          <Typography gutterBottom variant="h6" component="p" align="left">
            {gpuProduct.name} 
             <Typography variant="body2" color="textSecondary" component="p">
         {gpuProduct.highlightText} 
          </Typography>
             <Typography variant="h6" color="error" component="p"> 
            <span>&#8369;</span> {gpuProduct.price} 
          </Typography>
          </Typography>
         
   
      </CardActionArea>

    </Card>
        </Grid>

      
      </Grid>

    
</>

  );
}


