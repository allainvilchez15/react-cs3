/*import './App.css';*/
import React, {useState} from 'react';
import './App.css';
import Register from './pages/SignUpPage';
import Register1 from './pages/UpPage';
/*import Logout from './components/Logout'*/
import Login from './pages/Login';
import ProductPage from './pages/productPage';
import ProductPagee from './pages/ProductPages';
import NotFound from './pages/NotFound';
import OrderPage from './pages/OrderPage';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import AddProduct from './components/admindashboard/AddProduct';
import Profile from './pages/ProfilePage'

import Navbar from './components/Navbar';
import Footer from './components/Footer';
import UserContext from './UserContext';
import ViewProduct from './components/ViewProduct'
import ViiewProduct from './components/ViewProduct1'
import ViewTransaction from './components/viewTransactions'

import {Container} from 'react-bootstrap';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './Body.css';



function App() {
  const [user, setUser] = useState
  ({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  });

  const unsetUser = () => {

    localStorage.clear();
    setUser
    ({
      accessToken: null,
      iAdmin: null
    });
  }
/*if (Login) {*/

  return (

  <UserContext.Provider value={{user, setUser, unsetUser}}>
    <BrowserRouter> 
      <Navbar/>
       <Container>
        <Switch>
           <Route exact path="/" component={Home} />
           <Route exact path="/login" component={Login} />
           <Route exact path="/register" component={Register} />
           <Route exact path="/register1" component={Register1} />
           <Route exact path="/transactions" component={OrderPage} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/admin/addprodduct" component={AddProduct} />
            <Route exact path="/transactions/:id" component={ViewTransaction} />
            <Route exact path="/product" component={ProductPage} />
           <Route exact path="/products" component={ProductPagee} />
           <Route exact path="/product/:id" component={ViewProduct} />
            <Route exact path="/products/:id" component={ViiewProduct} />
           <Route exact path="/profile" component={Profile} />
            <Route component={NotFound} />


        
       </Switch>

       </Container>
         <Footer/>
    </BrowserRouter> 
  </UserContext.Provider> 

  );

}

export default App;
