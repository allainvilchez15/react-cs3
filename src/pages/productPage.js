import React, { useContext, useEffect, useState } from "react";
import Products from "../components/Products"; //This component will serve as the storage of the data upon displaying it in the browser
import MoboProducts from "../components/moboProducts";
import CpuProducts from "../components/cpuProducts";
import GpuProducts from "../components/gpuProducts";
/*import coursesData from "../data/courses";*/ // to acquire the actual data that we want to display, describes all the courses records farom the data folder.
import UserContext from "../UserContext";
import { Table } from "react-bootstrap";
import Grid from "@material-ui/core/Grid";
import ViewButton from "../components/ViewProduct";
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';



/*import { Redirect, useHistory } from "react-router-dom";*/

export default function ProductPage() {
/*	const history = useHistory();*/

	const [allProducts, setAllProducts] = useState([]);
	const [allGPU, setAllGPU] = useState([]);
	const [allCPU, setAllCPU] = useState([]);
	const [allMOBO, setAllMOBO] = useState([]);


/*ALLRPDOCUTS*/
	const { user } = useContext(UserContext);

	useEffect(() => {
		fetch("http://localhost:4000/getAllProduct", {
			headers: {
				Authorization: `Bearer $(user.accessToken)`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
		
		localStorage.removeItem('product', data._id);
					setAllProducts(
						data.map((product) => {
							return (
								<tr key={product._id}>
									<th>
										<Products
											key={product.id}
											product={product}
											productId={product._id}
										/>

										<Link to={`/product/${product._id}`} style={{ textDecoration: 'none' }}>
  										<Button size="small" variant="outlined" color="primary" align="left" fullWidth>View Product</Button>
										</Link>
										
									</th>

								</tr>
							);
						})

					);
		
	})
			/*MOTHERBOARD*/
fetch("http://localhost:4000/getAllMOBO", {
			headers: {
				Authorization: `Bearer $(user.accessToken)`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				console.log(data);

		
					setAllMOBO(
						data.map((moboProduct) => {
							return (
								<tr key={moboProduct._id}>
									<th>
										<MoboProducts
											key={moboProduct.id}
											moboProduct={moboProduct}
											moboProductId={moboProduct._id}
										/>
										<Link to={`/product/${moboProduct._id}`} style={{ textDecoration: 'none' }}>
  										<Button size="small" variant="outlined" color="primary" align="left" fullWidth>View Product</Button>
										</Link>
									</th>

								</tr>
							);
						})
					);
		
			});

/*CPU*/
			fetch("http://localhost:4000/getAllCPU", {
			headers: {
				Authorization: `Bearer $(user.accessToken)`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				console.log(data);

		
					setAllCPU(
						data.map((cpuProduct) => {
							return (
								<tr key={cpuProduct._id}>
									<th>
										<CpuProducts
											key={cpuProduct.id}
											cpuProduct={cpuProduct}
											cpuProductId={cpuProduct._id}
										/>
										<Link to={`/product/${cpuProduct._id}`} style={{ textDecoration: 'none' }}>
  										<Button size="small" variant="outlined" color="primary" align="left" fullWidth>View Product</Button>
										</Link>
									</th>

								</tr>
							);
						})
					);
		
		
			});

						/*GPU*/
fetch("http://localhost:4000/getAllGPU", {
			headers: {
				Authorization: `Bearer $(user.accessToken)`,
			},
		})
			.then((response) => response.json())
			.then((data) => {
				console.log(data);

		
					setAllGPU(
						data.map((gpuProduct) => {
							return (
								<tr key={gpuProduct._id}>
									<th>
										<GpuProducts
											key={gpuProduct.id}
											gpuProduct={gpuProduct}
											gpuProductId={gpuProduct._id}
										/>
										        <Link to={`/product/${gpuProduct._id}`} style={{ textDecoration: 'none' }}>
  										<Button size="small" variant="outlined" color="primary" underline="none" align="left" fullWidth>View Product</Button>
										</Link>
									</th>

								</tr>
							);
						})
					);
		
			});




	}, []);



		return (
			<>
				<h1 className="text-center mb-5">Storefront</h1>
			
				<Table>
					<tbody>
			
					<tr>
							<th>Motherboard
							{/*{allProducts}*/} {allMOBO}</th>
							<th>Video Cards
							{allGPU}</th>
							<th>CPU
							{allCPU}</th>
						</tr>
				
					</tbody>
				</Table>

			</>
		);

}
