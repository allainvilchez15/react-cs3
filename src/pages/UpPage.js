import React, {useState, useEffect, useContext} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Swal from 'sweetalert2/src/sweetalert2.js'
import UserContext from '../UserContext';
import {Redirect, useHistory} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import * as AiIcons from 'react-icons/ai';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Profile from '../assets/images/RE.jpeg'
import '../assets/css/Signup.css'


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 105,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),

  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Register() {
  const history = useHistory();
  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setmobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');
  const {user} = useContext(UserContext);
  const classes = useStyles();
  const[registerButton, setRegisterButton] = useState(false);


  useEffect(() => {
    const abortController = new AbortController()


    if((email !== '' && firstName !== '' && lastName !== ''&& mobileNo !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
        setRegisterButton(true);
      
      } else {
        setRegisterButton(false);
      
      } 

      return function cleanup() {
        abortController.abort()
      }
  }, [email, password])


function registerUser (e){

  e.preventDefault(); 

 fetch('http://localhost:4000/adminCreatePage', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json'},
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      mobileNo: mobileNo,
      email: email,
      password: password
    })
  })
  .then(response => response.json())
  .then(data => {
    console.log(data)
    if (data === true) {
  Swal.fire({
      
         /*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    title: "Registered Success - Redirecting",
    text: "Login Page",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,

      })
history.push('/login')
} else {


      Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    text: "Error Check Credentials",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
  })

    }
        setfirstName('');
      setlastName('');
      setmobileNo('');
      setEmail('');
      setPassword('');
      setVerifyPassword('');
    

}
 
    );

    

  
    }
  if(user.accessToken !== null) {
    return <Redirect to="/" />

  };
  





  return (


    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
           <div class="fa-2x">
     <i class="far fa-life-ring fa-spin"></i>
       </div>
        <Typography component="h1" variant="h5" color="initial">
          User Registration
        </Typography>
        <form onSubmit={(e) => registerUser(e)} align="left">
          <TextField
          className = "textfield"
          color="primary"
            variant="outlined"
            margin="normal"
            fullWidth
            id="firstName"
            label="First Name"
            name="firstName"
            autoComplete="email"
            value={firstName} 
            onChange={e => setfirstName(e.target.value)} 
            autoFocus


          />

          <TextField
          className = "textfield"
          color="primary"
            variant="outlined"
            margin="normal"
            fullWidth
            id="lastName"
            label="Last Name"
            name="lastName"
            autoComplete="lastName"
            value={lastName} 
            onChange={e => setlastName(e.target.value)} 
            autoFocus
          />
          <TextField
          className = "textfield"
          color="primary"
            variant="outlined"
            margin="normal"
            fullWidth
            name="email"
            label="Email"
            type="email"
            id="email"
            value={email} 
            onChange={e => setEmail(e.target.value)} 

          />

          <TextField
          className = "textfield"
          color="primary"
            variant="outlined"
            margin="normal"
            fullWidth
            name="mobileNo"
            label="Contact Number"
            type="mobileNo"
            id="mobileNo"
            value={mobileNo} 
            onChange={e => setmobileNo(e.target.value)} 
            

          />

          <TextField
          className = "textfield"
          color="primary"
            variant="outlined"
            margin="normal"
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={password} 
            onChange={e => setPassword(e.target.value)} 

          />

          <TextField
          className = "textfield"
          color="primary"
            variant="outlined"
            margin="normal"
            fullWidth
            name="verifyPassword"
            label="Verify Password"
            type="password"
            id="verifyPassword"
            value={verifyPassword} 
            onChange={e => setVerifyPassword(e.target.value)} 

          />


          {
      registerButton 
          ?
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
          >
            Register
          </Button>
          :
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            disabled
          >
            Check Credentials 
          </Button>
          }
         
         
        </form>

      </div>
    </Container>

  );
}