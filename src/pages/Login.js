import React, {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2/src/sweetalert2.js'
import UserContext from '../UserContext';
import {Redirect, useHistory} from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import * as AiIcons from 'react-icons/ai';
import '../components/Navbar.css';




const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 125,
    marginBottom: 185,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login() {
  const history = useHistory();
  const {user, setUser} = useContext(UserContext);
  
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loginButton, setLoginButton] = useState(false);
  const classes = useStyles();


  useEffect(() => {
    const abortController = new AbortController()


    if(email !== '' && password !== '' ) {
        setLoginButton(true);
      
      } else {
        setLoginButton(false);
      
      } 

      return function cleanup() {
        abortController.abort()
      }
  }, [email, password])


function loginUser(e){

  e.preventDefault(); 

  fetch('http://localhost:4000/login', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json'},
    body: JSON.stringify({
      email: email,
      password: password
    })
  })
  .then(response => response.json())
  .then(data => {
    console.log(data)
    //let's do the response in our data once we receive it
    if(data.accessToken !== undefined) {
      localStorage.setItem('accessToken', data.accessToken );
      setUser({ accessToken: data.accessToken});

       Swal.fire({
/*    toast: true,*/
    icon: 'success',
    iconHtml: '<i class="far fa-compass fa-spin"></i>',
    text: "User Login - Redirecting",
   /* background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,

  }).then(() => history.push('/product'))




      //get users details from our token
      fetch('http://localhost:4000/detailsUser1', {
        headers: {
          Authorization: `Bearer ${data.accessToken}`
        }
      })
      .then(response => response.json())
      .then(data => {
        console.log(data)


      if(localStorage !== null ){
            localStorage.setItem('email', data.email);
            localStorage.setItem('firstName', data.firstName);
            localStorage.setItem('lastName', data.lastName);
            localStorage.setItem('isAdmin', data.isAdmin);
            localStorage.setItem('id', data._id);
            

            setUser({
              email: data.email,
              firstName: data.firstName,
              isAdmin: data.isAdmin,
              id: data._id
            })
            //what router can we use to redirect the page to /courses if isAdmin is true?
            
          }


      })
    } else {
      Swal.fire({
/*    toast: true,*/
    icon: 'error',
    iconHtml: '<i class="fas fa-times fa-spin"></i>',
    text: "Error Check Credentials",
/*    background: '#fff url(https://digitalsynopsis.com/wp-content/uploads/2017/03/beautiful-color-gradients-backgrounds-078-cochiti-lake.png)',*/
    position: 'center',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
  })

    }
      setEmail('');
      setPassword('');


  });
  }





  return (
    <Container className="main" component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
     
     <div className='login-icon'>
          <AiIcons.AiOutlineUserAdd />
       </div>
        <Typography component="h1" variant="h5" color="initial">
          User Sign in
        </Typography>
        <form onSubmit={(e) => loginUser(e)}>
          <TextField
          className = "textfield"
          color="secondary"
            variant="filled"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={email} 
            onChange={e => setEmail(e.target.value)} 
            required
            autoFocus
          />
          <TextField
          className = "textfield"
          color="secondary"
            variant="filled"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={password} 
            onChange={e => setPassword(e.target.value)} 
            required

          />


          {
      loginButton 
          ?
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
          >
            Sign In
          </Button>
          :
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            disabled
          >
            Check Credentials to Login
          </Button>
          }
         
         
        </form>
      </div>
    </Container>
  );
}