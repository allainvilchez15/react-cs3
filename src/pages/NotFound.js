import React from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/NotFound.css';




export default function NotFound() {

	return(
 <div className="NA">
  <center>
    <h1>404 Error - Page Not Found!</h1>
    <Link to="/">
      Go Home
    </Link>

    </center>
  </div>

		);
}