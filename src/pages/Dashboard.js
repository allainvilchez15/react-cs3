import React, { useState, useContext } from 'react';
import UserContext from '../UserContext';
import {Redirect, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Zoom from '@material-ui/core/Zoom';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import { green } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import UsersPanel from '../components/admindashboard/UsersPanel';
import ProductsPanel from '../components/admindashboard/ProductPanel';
import TransactionsPanel from '../components/admindashboard/viewAllTransaction'
import AdminPanel from '../components/admindashboard/AdminPanel';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import FileCopyIcon from '@material-ui/icons/FileCopy';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`action-tabpanel-${index}`}
      aria-labelledby={`action-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `action-tab-${index}`,
    'aria-controls': `action-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    position: 'relative',
    minHeight: 200,
  },
}));



  export default function Dashboard() {
  const {user} = useContext(UserContext);
/*  const history = useHistory();*/
/*history.push('/')*/
 const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };



  if(user.isAdmin === false) {
    return <Redirect to="/" />
  }



 return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="secondary"
          variant="fullWidth"
          aria-label="action tabs example"
        >
          <Tab label="USERS PANEL" icon={<PersonPinIcon />} {...a11yProps(0)} />
          <Tab label="PRODUCTS PANEL" icon={<CardGiftcardIcon />} {...a11yProps(1)} />
          <Tab label="TRANSACTIONS PANEL" icon={<FileCopyIcon />} {...a11yProps(2)} />
          <Tab label="ADMIN PANEL" icon={<FileCopyIcon />} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
        <UsersPanel/>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
        <ProductsPanel/>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
        <TransactionsPanel/>
        </TabPanel>
        <TabPanel value={value} index={3} dir={theme.direction}>
        <AdminPanel/>
        </TabPanel>
      </SwipeableViews>
    <Typography color="default" variant="inherit"> Admin {localStorage.firstName} {localStorage.lastName} {localStorage.id} </Typography>
    </div>
  );


}
